class TestBugF1 {
    public static void main(String[] args) {
        System.out.println(new Test().b1());
    }
}

class Test {
    public int b1() {
        int result;
        int x;
        int y;
        x = 7;
        y = 7;
        result = 0;
        
        if(x || y) {
          result = result + 1;
        }
        else{
          result = result - 1;
        }
        return result;
      }
}
